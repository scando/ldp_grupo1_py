# -*- coding: utf-8 -*-
"""
Created on Wed Nov 26 15:01:44 2014

@author: kevin
"""

import aes
from usuario import *

class ArchiveHandler():
    def __init__(self):
        self.__claveAdmin__ = 'This is a key123'
    
    def isContraAdminValid(self, contraActual):
        if(self.__claveAdmin__ == contraActual):
            return True
            
    def escribirArchivo(self, filePath):
        archivo = open(filePath, "ab")
        acum = ""
        
        for usuario in UsuarioList:
            text = usuario.toString()            
            acum = text + acum
            print acum
        texto = aes.encryptData(self.__claveAdmin__, acum)
        archivo.write(texto)
        
    def leerArchivo(self, filePath):
        try:        
            archivo = open(filePath,"rb")
        except IOError:
            print "Hubo un problema al leer el archivo"
        else:    
            lineas = archivo.read()
            textoDesencrip = aes.decryptData(self.__claveAdmin__, lineas)
            texto = textoDesencrip.split('\n')
            
            for linea in texto:
                 if linea != '':
                     user = read(linea)
                     UsuarioList.append(user)


if __name__ == "__main__":
    pass
