# -*- coding: utf-8 -*-
"""
Created on Tue Dec  9 00:34:45 2014

Adapted from:
    http://codeplasma.com/2012/12/03/getting-webcam-images-with-python-and-opencv-2-for-real-this-time/
    
@author: kevin
"""

import cv2
 

cascPath2 = './Haarcascade/haarcascade_frontalface_alt2.xml'

def tomarFoto(fotopath):
    faceCascade = cv2.CascadeClassifier(cascPath2)
     
    # Camera 0 is the integrated web cam on my netbook
    camera_port = 0
     
    #Number of frames to throw away while the camera adjusts to light levels
    ramp_frames = 30
     
    # Now we can initialize the camera capture object with the cv2.VideoCapture class.
    # All it needs is the index to a camera port.
    camera = cv2.VideoCapture(camera_port)
     
    # Captures a single image from the camera and returns it in PIL format
    def get_image():
         # read is the easiest way to get a full image out of a VideoCapture object.
         retval, im = camera.read()
         return im
     
    # Ramp the camera - these frames will be discarded and are only used to allow v4l2
    # to adjust light levels, if necessary
    for i in xrange(ramp_frames):
     temp = get_image()
     
    
    print("Taking image...")
    # Take the actual image we want to keep
    camera_capture = get_image()
    
    file = fotopath + ".jpg"
    
    gray = cv2.cvtColor(camera_capture, cv2.COLOR_BGR2GRAY)
    # Detect faces in the image
    faces = faceCascade.detectMultiScale(
    		gray,
    		scaleFactor=1.1,
    		minNeighbors=5,
    		minSize=(30, 30),
    		flags = cv2.cv.CV_HAAR_SCALE_IMAGE
    	)
    print "Found {0} faces!".format(len(faces))
    # Draw a rectangle around the faces
    startY = 0
    endY = 0
    startX = 0
    endX = 0
    
    for (x, y, w, h) in faces:
         startX = x
         startY = y
         endY = y+h
         endX = x+w
         cv2.rectangle(camera_capture, (x, y), (x+w, y+h), (0, 255, 0), 1) 
    	
    cropped = camera_capture[startY:endY, startX:endX]
    
    # A nice feature of the imwrite method is that it will automatically choose the
    # correct format based on the file extension you provide. Convenient!
    cv2.imwrite(file, cropped)
    # You'll want to release the camera, otherwise you won't be able to create a new
    # capture object until your script exits
    
    return file