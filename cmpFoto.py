# -*- coding: utf-8 -*-
"""
Created on Mon Dec 15 12:15:26 2014

@author: kevin
"""

# import the necessary packages
from skimage.measure import structural_similarity as ssim
import matplotlib.pyplot as plt
import numpy as np
import cv2

def mse(imageA, imageB):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err
 
def compare_images(imageA, imageB, frame = None):
    # compute the mean squared error and structural similarity
    # index for the images
    m = 0
    if frame is None: 
        #m = mse(imageA, imageB)
        s = ssim(imageA, imageB)
    
        if m < 1500 and s > float(0.70):
            return True
            
        else:
            return False
    else:
        m = mse(imageA, frame)
        s = ssim(imageA, frame)
    
        if m < 1500 and s > float(0.70):
            return True
            
        else:
            return False
'''
    imagePath1 es la imagen que se desea verificar, imagePath2 es la imagen que se tomo
    y que se desea comprobar y por ultimo frame es un parametro si es que viene de video
    
'''
def img_cmp(imagePath1, imagePath2, frame = None):
    # load the images -- the original, the original + contrast,
    # and the original + photoshop
    original = cv2.imread(imagePath1)
    diferent = cv2.imread(imagePath2)
     
    # convert the images to grayscale
    original = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
    diferent = cv2.cvtColor(diferent, cv2.COLOR_BGR2GRAY)
    
    return compare_images(original,diferent, frame)    