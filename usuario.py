# -*- coding: utf-8 -*-
"""
Created on Tue Nov 25 16:09:33 2014

@author: kevin
"""

from gi.repository import Gtk
from Clave import Clave

#Variables Globales de la clase Usuario
USUARIO_FILE = "Users.txt"
UsuarioList = []

class Usuario():
    '''
        `La clase usuario es la que le permirte al programa manejar como obtener los valores de los
        usuarios ingresados al sistema.
         - Se le ha hecho metodos set y get asi mismo como un metodo toString. 
         -la funcionalidad viene prevista de como se use la clase
    '''
    
    def __init__(self,Id,nombre,apellido,claveNum,claveAlfNum,imagePath = ""):
        try:        
            self.__id = int(Id)
            self.__nombre = str(nombre)
            self.__apellido = str(apellido)
            self.__claveNum = int(claveNum)
            self.__claveAlfNum = str(claveAlfNum)
            self.__imagePath = str(imagePath)
        except ValueError:
           __errorMessage()
    '''
        Funciones de metodos para funciones get y set
    '''
    
    def getid(self):
        return self.__id
        
    def getNombre(self):
        return self.__nombre
        
    def getApellido(self):
        return self.__apellido
    
    def getClaveNum(self):
        return self.__claveNum
    
    def getClaveAlfNum(self):
        return self.__claveAlfNum

    def getImagenPath(self):
        return self.__imagePath
                
    def setid(self, Id):
        try:
            self.__id = int(Id)
        except ValueError:
            __errorMessage()
    
    def setNombre(self, nombre):
        try:
            self.__nombre = str(nombre)
        except ValueError:
            __errorMessage()
        
    def setApellido(self, apellido):
        try:
            self.__apellido = str(apellido)
        except ValueError:
            __errorMessage()

    def setClaveNum(self, claveNum):
        try:
            self.__claveNum = int(claveNum)
        except ValueError:
            __errorMessage()

    def setClaveAlfNum(self, claveAlfNum):
        try:
            self.__claveAlfNum = str(claveAlfNum)
        except ValueError:
            __errorMessage()


    def setImagePath(self, imagePath):
        try:
            self.__imagePath = str(imagePath)
        except ValueError:
            __errorMessage()
        
    '''
        Metodos toString y compareTo 
        Metodo toString(self)
        @return Retorna los valores de los campos en el usuario en un string
    '''
    
    def toString(self):
        delim = ','
        fin = '\n'
        return str(self.__id) + delim + self.__nombre + delim + self.__apellido + delim + str(self.__claveNum) + delim +self.__claveAlfNum + fin
        
    def compareTo(self, usuario2):
        try:
            if isinstance(usuario2,Usuario) :
                if self.__id == user.__id:
                    return True
                else: 
                    return False
            elif isinstance(usuario2,str):
                print usuario2
                if int(usuario2) == int(self.__id):
                    return True
                else:
                    return False
        except ValueError:
            __errorMessage()
            

def __errorMessage():
    dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR,
        Gtk.ButtonsType.OK, "Tipo de dato incorrecto")
    
    dialog.run()
    
    dialog.destroy()
    
def usuarioExist(usuario2):
    for user1 in UsuarioList:
        print user1.toString()
        if(user1.compareTo(usuario2) == True):
            return user1
        else:
            None

def leerLinea(linea):
    try:        
        ln = str(linea)        
    except ValueError:
        __errorMessage()
    return ln.split(',')    

#Esta funcion permite manejar la lectura de archivo de la clase Usuario
def read(linea):
    Linea = leerLinea(linea)
    return Usuario(int(Linea[0]),Linea[1],Linea[2],int(Linea[3]),Linea[4])
    
if __name__ == "__main__":
    cont = 0  

    while cont < 2:
        print "Ingrese un nombre"
        nom = raw_input()
        print "Ingrese un apellido"
        ape = raw_input()
        clav = Clave()
        user = Usuario(cont,nom, ape, clav.num, clav.alfa)
        UsuarioList.append(user)
        cont += 1
        
    for user in UsuarioList:
        print user.getNombre