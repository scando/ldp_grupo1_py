# -*- coding: utf-8 -*-
"""
Created on Wed Dec  3 14:03:11 2014

@author: kevin
"""
import threading
from camScript import *
from gi.repository import Gtk, GObject, GdkPixbuf, Gdk
from Clave import *
import Archivo
from usuario import *
from cmpFoto import *
import patternRecog

UI_FILE = "./UI_FILES/VtnUserInterface.ui"
ANIMATION = "./data/banco.gif"

CSS_STYLE = "./CSS_STYLES/vtnUser.css"
CSS_RESET = "./CSS_STYLES/reset.css"

builder = Gtk.Builder()
builder.add_from_file(UI_FILE)

#Se genera una matriz completamente aleatoria
Matrix = GenerarMatriz()  

class VtnUser():
    def __init__(self, thread):
        self.vtnUser = builder.get_object('VtnUser')
        self.revealer = builder.get_object('revealer1')
        self.revealer2 = builder.get_object('revealer2')
        self.lblPerso = builder.get_object('lblPersonalizar')
        self.imagen = builder.get_object('banco')
        
        self.btn1 = builder.get_object('btn1')
        self.btn2 = builder.get_object('btn2')
        self.btn3 = builder.get_object('btn3')
        self.btn4 = builder.get_object('btn4')
        self.btn5 = builder.get_object('btn5')
        self.btn6 = builder.get_object('btn6')
        
        self.lblTxt1 = builder.get_object('lblTxt1')
        self.lblTxt2 = builder.get_object('lblTxt2')
        self.lblTxt3 = builder.get_object('lblTxt3')
        self.lblTxt4 = builder.get_object('lblTxt4')
        self.lblTxt5 = builder.get_object('lblTxt5')
        self.lblTxt6 = builder.get_object('lblTxt6')
        
        self.usuario = Usuario(0,"","",0,"")
        
        self.buttonCount = 0        
        
        self.clave1 = False
        self.clave2 = False
        self.clave3 = False
        self.clave4 = False
        
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "Ventana Usuario"
        self.vtnUser.set_titlebar(hb)
        
        pixbuf = GdkPixbuf.PixbufAnimation.new_from_file(ANIMATION)
        self.imagen.set_from_animation(pixbuf)
        
        self.handlerId = self.btn6.connect("clicked",self.btn6_clicked_cb)
        
        self.vtnUser.connect("destroy", lambda w: Gtk.main_quit())
        
        builder.connect_signals(self)
        self.setStyle(CSS_STYLE, self.vtnUser)
        #self.setStyle(CSS_STYLE, self.revealer)
        
    def button_press_event_cb(self, widget,event):
        dialog = Gtk.Dialog(title='Interactive Dialog',
                            transient_for=self.vtnUser,
                            modal=True,
                            destroy_with_parent=True)
        dialog.add_buttons(Gtk.STOCK_OK, Gtk.ResponseType.OK,
                           Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)

        content_area = dialog.get_content_area()
        hbox = Gtk.HBox(spacing=8)
        hbox.set_border_width(8)
        content_area.pack_start(hbox, False, False, 0)

        stock = Gtk.Image(stock=Gtk.STOCK_DIALOG_QUESTION,
                          icon_size=Gtk.IconSize.DIALOG)

        hbox.pack_start(stock, False, False, 0)

        table = Gtk.Table(n_rows=2, n_columns=1, homogeneous=False)
        table.set_row_spacings(4)
        table.set_col_spacings(4)
        hbox.pack_start(table, True, True, 0)
        label = Gtk.Label.new_with_mnemonic("_Ingrese el id del usuario")
        table.attach_defaults(label, 0, 1, 0, 1)
        local_entry1 = Gtk.Entry()
        table.attach_defaults(local_entry1, 1, 2, 0, 1)
        label.set_mnemonic_widget(local_entry1)

        hbox.show_all()

        response = dialog.run()   
        if  response == Gtk.ResponseType.OK:
            self.setStyle(CSS_RESET, self.revealer2)
            self.setStyle(CSS_STYLE, self.revealer2)
            self.usuario = usuarioExist(local_entry1.get_text())
            if self.usuario != None:
                self.lblPerso.set_text("Bienvenido," + self.usuario.getNombre() + "\nPor favor ingresa tu contrasena")
                password = self.usuario.getClaveAlfNum().split()
                texto1 = casillasAleatorias(0,range(5),password[0],password[3])
                self.lblTxt1.set_text(texto1)      
                texto2 = casillasAleatorias(0,range(5,10),password[1])             
                self.lblTxt2.set_text(texto2)
                texto3 = casillasAleatorias(1,range(5),password[2],password[3])            
                self.lblTxt3.set_text(texto3)
                self.lblTxt4.set_text(casillasAleatorias(1,range(5,10),password[0],password[3]))
                self.lblTxt5.set_text(casillasAleatorias(2,range(5),password[1]))
                self.lblTxt6.set_text(casillasAleatorias(2,range(5,10),password[2],password[3]))            
                
                if bool(self.revealer.get_reveal_child()) is True:
                    self.revealer2.set_reveal_child(True)
                    self.revealer.set_reveal_child(False)
                    
        dialog.destroy()      
        
    def setStyle(self, cssPath, widget):
        
        #Se carga el estilo a ser utilizado
        data = open(cssPath,'rb')
        byte = data.read()
        data.close()        
        provider = Gtk.CssProvider()
        provider.load_from_data(byte)
        
        self.apply_css(widget,provider)
        
    def apply_css(self, widget, provider):
        Gtk.StyleContext.add_provider(widget.get_style_context(),
                                      provider,
                                      Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        if isinstance(widget, Gtk.Container):
            widget.forall(self.apply_css, provider)        
    
    '''
        Aqui se verifica el ingreso de la secuencia de botones presionados sin
        importar el orden al usuario 
    '''
    
    def verificarClave(self, widget):
        lbl = widget
        password = self.usuario.getClaveAlfNum().split()
        
        palabras = lbl.get_text().split()
        self.buttonCount += 1
        print self.buttonCount
        
        print palabras
        print password        
        
        for word in palabras:
            if word == password[0]:
                self.clave1 = True
            elif word == password[1]:
                self.clave2 = True
            elif word == password[2]:
                self.clave3 = True
            elif word == password[3]:
                self.clave4 = True
                
        if self.buttonCount >= 4: 
            if self.clave1 == True and self.clave2 == True and self.clave3 == True:
                self.accesoAutorizado()
        
    def btn1_clicked_cb(self, widget):
        self.verificarClave(self.lblTxt1)
                
    def btn2_clicked_cb(self, widget):
        self.verificarClave(self.lblTxt2)
    
    def btn3_clicked_cb(self, widget):
        self.verificarClave(self.lblTxt3)
    
    def btn4_clicked_cb(self, widget):
        self.verificarClave(self.lblTxt4)
    
    def btn5_clicked_cb(self, widget):
        self.verificarClave(self.lblTxt5)
    
    def btn6_clicked_cb(self, widget):
        self.verificarClave(self.lblTxt6)
    
    def accesoAutorizado(self):
        self.buttonCount = 0
        self.clave1 = False
        self.clave2 = False
        self.clave3 = False
        self.clave4 = False
        self.lblPerso.set_text("Bienvenido," + self.usuario.getNombre() )
        self.lblTxt1.set_text("Consulta de saldos")
        self.lblTxt2.set_text("Historial")
        self.lblTxt3.set_text("Pagos")
        self.lblTxt4.set_text("Retiros")
        self.lblTxt5.set_text("Transferencias")        
        self.lblTxt6.set_text("Salir")
        self.btn6.disconnect(self.handlerId)
        self.btn6.connect("clicked", self.btn6_salir_cb)

        thread.start()
        while thread.is_alive():
            Gtk.main_iteration_do(True)    
        if thread.is_alive() == False:
            self.callback()
            
    def btn6_salir_cb(self,widget):
        self.callback()
        
    def callback(self):
        if thread.is_alive():
            thread.__stop()
        if bool(self.revealer2.get_reveal_child()) is True:
            self.revealer2.set_reveal_child(False)
            self.revealer.set_reveal_child(True)
        


def casillasAleatorias(fila, rangoColumnas, auxiliar = None, cuatroParam = None):
    acumula = ''    
    cont = 0
    cont2 = 0
    for i in rangoColumnas:
        if (1 == random.choice(range(3))) and cont == 0:
            if auxiliar is not None:            
                acumula += auxiliar
                cont += 1
                acumula += " "
        else:  
            if (1 == random.choice(range(5))) and cont2 == 0 and cuatroParam is not None:
                acumula += cuatroParam
                cont2 += 1
                acumula += " "
            else:
                acumula += Matrix[fila][i]
                acumula += " "
    return acumula
    
if __name__ is "__main__" :
  
    GObject.threads_init()
    
    thread = threading.Thread(target=actividad)
    thread.daemon = False

    archiv = Archivo.ArchiveHandler()
    archiv.leerArchivo(USUARIO_FILE)
   
    vtn = VtnUser(thread)
    vtn.vtnUser.show_all()
    
    Gtk.main()