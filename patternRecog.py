# -*- coding: utf-8 -*-
"""
Created on Sun Dec 14 16:03:18 2014

@author: kevin
"""

import cv2 

cascPath2 = './Haarcascade/haarcascade_frontalface_alt2.xml'

def actividad():
    faceCascade = cv2.CascadeClassifier(cascPath2)
    
    # Camera 0 is the integrated web cam on my netbook
    camera_port = 0
     
    #Number of frames to throw away while the camera adjusts to light levels
    ramp_frames = 30
     
    #def targetFunct(flag):
    # Now we can initialize the camera capture object with the cv2.VideoCapture class.
    # All it needs is the index to a camera port.
    camera = cv2.VideoCapture(camera_port)
     
    # Captures a single image from the camera and returns it in PIL format
    def get_image():
         # read is the easiest way to get a full image out of a VideoCapture object.
         retval, im = camera.read()
         return im
     
    # Ramp the camera - these frames will be discarded and are only used to allow v4l2
    # to adjust light levels, if necessary
    for i in xrange(ramp_frames):
        temp = get_image()
    
    while True:
        
        # Take the actual image we want to keep
        camera_capture = get_image()
           
        gray = cv2.cvtColor(camera_capture, cv2.COLOR_BGR2GRAY)
        # Detect faces in the image
        faces = faceCascade.detectMultiScale(
        		gray,
        		scaleFactor=1.1,
        		minNeighbors=5,
        		minSize=(30, 30),
        		flags = cv2.cv.CV_HAAR_SCALE_IMAGE
        	)

        if len(faces) is 0:
            break
               