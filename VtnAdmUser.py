# -*- coding: utf-8 -*-
"""
Created on Thu Dec  4 01:25:41 2014

@author: kevin
"""
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib, Gio, GObject
import Clave
from usuario import *
from camScript import *

UI_FILE = "./UI_FILES/VtnAdmUserInterface.ui"

#builder.connect_signals(VtnAdmUser())

class VtnAdmUser():
    def __init__(self):
        
        builder = Gtk.Builder()
        builder.add_from_file(UI_FILE)
        self.vtnAdmUser = builder.get_object('VtnAdmUser')
        self.txtNombre = builder.get_object('txtNomb')
        self.txtApellido = builder.get_object('txtApell')
        self.txtID = builder.get_object('txtGETID')
        self.txtNumCont = builder.get_object('txtNumCont')
        self.txtCont = builder.get_object('txtCont')
        self.btnTomarFoto = builder.get_object('btnTomarFoto')
        self.btnAceptar = builder.get_object('btnAceptar')
        self.btnCancelar = builder.get_object('btnCancelar')
        self.btnGenerar = builder.get_object('btnGenerar')
        self.imagen = builder.get_object('imagenPersona')        
        self.box = builder.get_object('box5')        
        self.btnGenerarId = 0
        self.cont = 0        
        self.user = Usuario(0,"","",0,"")
        
        builder.connect_signals(self)
        btnAcep = self.vtnAdmUser.add_button("Aceptar",Gtk.ResponseType.ACCEPT)        
        btnAcep.connect('clicked',self.btnAceptar_clicked_cb)
        btnCanc = self.vtnAdmUser.add_button("Cancelar",Gtk.ResponseType.CANCEL)        
        btnCanc.connect('clicked',self.btnCancelar_clicked_cb)
        
    #Funcion que permite guardar los cambios al agregar una persona al Sistema
    def btnAceptar_clicked_cb(self, widget):
        #self.cont = self.cont+1                
        if self.btnGenerarId == 0:
                message = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, 
                                            Gtk.ButtonsType.OK, "Todavia no hay presionado generar")
                message.run()
                message.destroy()
        else:
            
            self.user = Usuario(0,"","",0,"")

            self.user.setid(int(self.cont))
            self.user.setNombre(self.txtNombre.get_text()) 
            self.user.setApellido(self.txtApellido.get_text())
            self.user.setClaveNum(int(self.txtNumCont.get_text()))
            self.user.setClaveAlfNum(self.txtCont.get_text())
                
            UsuarioList.append(self.user)
  
    def btnCancelar_clicked_cb(self, widget):
        dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.QUESTION,
            Gtk.ButtonsType.YES_NO, "Desea guardar los cambios")
        
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            print "Se quiso guardar"
            widget.destroy()
        elif response == Gtk.ResponseType.NO:
            print("QUESTION dialog closed by clicking NO button")
            widget.destroy()

        dialog.destroy()

    def btnGenerar_clicked_cb(self, widget, data = None):
        clav = Clave()
        self.btnGenerarId += 1
        self.cont = len(UsuarioList)+1
        self.txtID.set_text(str(self.cont))
        self.txtNumCont.set_text(str(clav.num))
        self.txtCont.set_text(str(clav.alfa))
        
    def btnTomarFoto_clicked_cb(self, widget, data = None):
        if self.btnGenerarId > 0:
            self.imagenPath = tomarFoto('./data/' + self.txtID.get_text())
    
            pixbuf = GdkPixbuf.Pixbuf.new_from_file(self.imagenPath)
            transparent = pixbuf.add_alpha(True, 0xff, 0xff, 0xff)
            self.imagen.set_from_pixbuf(transparent)
            self.imagen.queue_draw()
            self.user.setImagePath(self.imagenPath)
        else:
            print 'Primero asigne el id al usuario'
            

if __name__ == "__main__":
    Vtn = VtnAdmUser()
    Vtn.vtnAdmUser.show_all()
    Vtn.vtnAdmUser.connect('destroy', Gtk.main_quit)
    Gtk.main()