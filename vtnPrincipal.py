# -*- coding: utf-8 -*-
"""
Created on Thu Dec  4 22:58:31 2014

@author: kevin
"""

from gi.repository import Gtk,Gio
from Archivo import *
from usuario import *
from VtnAdmUser import *
from MessageRow import *

'''
    Esta es la ventana principal del lado del servidor la cual permite la manipulacion de usuarios, 
    asi mismo escribe los datos de manera encriptada usando el algoritmo AES 
    
'''
UI_FILE = "./UI_FILES/pygtk_foobar.ui"
CSS_STYLE = "./CSS_STYLES/css_accordion.css"
CSS_RESET = "./CSS_STYLES/reset.css"

#Gtk.builder permite cargar las interfaces de un archivo xml
builder = Gtk.Builder()
builder.add_from_file(UI_FILE)

usuarioList2 = []

class VtnPrincipal():
    def __init__(self):
        #Obtengo los respectivos widgets del builder        
        self.window = builder.get_object('VentanaPrincipal')
        self.listbox = builder.get_object('listbox')
        self.btnAgregarUsuario = builder.get_object('btnAgregarUsuario')
        self.btnEliminarUsr = builder.get_object('btnEliminarUsr')
        self.btnConectar = builder.get_object('btnConectar')
        self.vtnAbout = builder.get_object('vtnAbout')
        
        #Creacion de la ventana
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "VentanaPrincipal"
        self.window.set_titlebar(hb)
        self.window.set_default_size(600, 300)
        self.window.set_border_width(10)
        self.window.connect("destroy", lambda w: Gtk.main_quit())
        
        self.cont = 0
        
        #Se cargan los estilos CSS primero se llama al archivo Reset.css el cual setea los 
        # widgets        
        data = open(CSS_RESET,'rb')
        byte = data.read()
        data.close()        
        reset = Gtk.CssProvider()
        reset.load_from_data(byte)
        
        self.apply_css(self.window,reset)
        
        #Se carga el estilo a ser utilizado
        data = open(CSS_STYLE,'rb')
        byte = data.read()
        data.close()        
        provider = Gtk.CssProvider()
        provider.load_from_data(byte)
        
        self.apply_css(self.window,provider)
        
        self.actualizeListBox()
        
    def vtnAbout_close_cb(self, widget):
        widget.destroy()
    
    def btnAcercaDe_clicked_cb(self, widget):
        widget.show_all()
    
    def btnConectar_clicked_cb(self, widget):
        archivo = ArchiveHandler()
        archivo.escribirArchivo(USUARIO_FILE)
    
    def btnAgregarUsuario_clicked_cb(self, widget):
            Vtn = VtnAdmUser()
            response = Vtn.vtnAdmUser.run()
            if response == Gtk.ResponseType.ACCEPT:
                self.actualizeListBox()
                self.apply_css(self.window,provider)
                self.window.queue_draw()
                while Gtk.events_pending():
                    Gtk.main_iteration_do(True)
            Vtn.vtnAdmUser.destroy()         
    
    def apply_css(self, widget, provider):
        Gtk.StyleContext.add_provider(widget.get_style_context(),
                                      provider,
                                      Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        if isinstance(widget, Gtk.Container):
            widget.forall(self.apply_css, provider)        
            
    def btnEliminarUsr_clicked_cb(self, widget):
        pass
        
    def actualizeListBox(self):
        if self.cont == 0:    
            for i in range(len(UsuarioList)):
                usuario = UsuarioList[i]
                row = Gtk.ListBoxRow()
                #row.add(Gtk.Label(usuario.toString()))
                content = MessageRow(usuario)
                row.add(content.window1)
                self.listbox.add(row)
                row.show()
            self.cont += 1
        else:
          #  for i in range(len(UsuarioList)-1):
                usuario = UsuarioList[len(UsuarioList)-1]
                row = Gtk.ListBoxRow()
                #row.add(Gtk.Label(usuario.toString()))
                content = MessageRow(usuario)
                row.add(content.window1)
                self.listbox.add(row)
                row.show()
        
builder.connect_signals(VtnPrincipal())

if __name__ is "__main__" :
    archiv = ArchiveHandler()
    archiv.leerArchivo(USUARIO_FILE)
      
    ventana = VtnPrincipal()
    ventana.window.show_all()
    Gtk.main()
     