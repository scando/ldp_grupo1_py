# -*- coding: utf-8 -*-
"""
Created on Tue Nov 25 16:27:05 2014

@author: kevin
"""
import random

Alfabeto = ['A','B','C','D','E','F','G','H','I','J','K','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
vocales = ['a','e','i','o','u','']

Matriz = [[],[],[]]

class Clave():
    def __init__(self):
        self.num = self.generarClaveNum()
        self.GenerarMatriz()               
        self.alfa = self.generarClaveAlf(self.num)
     
    def generarClaveNum(self):
        self.x4 = random.randrange(len(vocales))
        self.x3 = random.randrange(0,9)
        self.x2 = random.randrange(0,9)
        self.x1 = random.randrange(1,9)
        return self.x1*1000 + self.x2*100 + self.x3*10 + self.x4
        
    def generarClaveAlf(self, claveNum):
        suma = self.x1 + self.x2 + self.x3 + self.x4
        j = 0
        for i in range(suma):    
            if i >= len(Alfabeto):
                j = i
                i = 0
            else:
                i += 1
        
        p4 = Alfabeto[i] + Alfabeto[j].lower()
        p1 = Matriz[0][self.x1]
        p2 = Matriz[1][self.x2]
        p3 = Matriz[2][self.x3]
        
        return p1 + " " + p2 + " " + p3 + " " + p4
        
        
    def GenerarMatriz(self):
        for i in range(3):
            for j in range(10):
                Matriz[i].append(random.choice(Alfabeto) + vocales[self.x4])

def GenerarMatriz():
   RandomMatrix = [[],[],[]] 
   for i in range(3):
       for j in range(10):
           RandomMatrix[i].append(random.choice(Alfabeto) + random.choice(vocales))
   return RandomMatrix
   
if __name__ is "__main__":
    
    clave = Clave()
    print len(Alfabeto)
    print GenerarMatriz()
    print clave.num
    print clave.alfa