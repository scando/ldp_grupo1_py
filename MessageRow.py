# -*- coding: utf-8 -*-
"""
Created on Fri Dec 12 20:59:17 2014

@author: kevin
"""

from gi.repository import Gtk, GdkPixbuf
from usuario import *

UI_FILE = "./UI_FILES/messageRow.ui"

                
class MessageRow():
    def __init__(self, usuario):
        user = usuario
        #Se usa el Builder para cargar la interfaz desde el XMl
        builder = Gtk.Builder()
        builder.add_from_file(UI_FILE)
        
        self.window1 = builder.get_object('grid1')
        self.GtkMessageRow = builder.get_object('grid1')
        self.revealer = builder.get_object('revealer1')
        self.txtNombre = builder.get_object('txtNombre')
        self.txtApellido = builder.get_object('txtApellido')
        self.txtId = builder.get_object('txtId')
        self.txtClave = builder.get_object('txtClave')
        self.txtAlfa = builder.get_object('txtAlfa')
        self.imagen = builder.get_object('imgUser')
        
        #Se conectan las senales
        builder.connect_signals(self)
        
        #try:
        self.txtNombre.set_text(user.getNombre())
        self.txtApellido.set_text(user.getApellido())
        self.txtClave.set_text(str(user.getClaveNum()))
        self.txtAlfa.set_text(user.getClaveAlfNum())
        self.txtId.set_text(str(user.getid()))
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./data/" + str(user.getid()) + ".jpg",100,100,True)
        self.imagen.set_from_pixbuf(pixbuf)
            
        #except ValueError:
         #   print "Error"
        
    def btnClaves_clicked_cb(self, widget):
        if bool(self.revealer.get_reveal_child()) is True:
            self.revealer.set_reveal_child(False)
            widget.set_label("Claves")
            self.window1.queue_draw()
        else: 
            self.revealer.set_reveal_child(True)
            widget.set_label("Ocultar")
    
    def setStyle(self, cssPath):
        pass
    
    def layout(self):
        return self.window1