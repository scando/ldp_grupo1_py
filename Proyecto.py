# -*- coding: utf-8 -*-
"""
Created on Wed Nov 19 22:43:57 2014

@author: kevin
"""
from gi.repository import Gtk
import os, sys
from usuario import *
import Clave
from Archivo import *
import VtnAdmUser

#Comment the first line and uncomment the second before installing
#or making the tarball (alternatively, use project variables)
UI_FILE = "pygtk_foobar.ui"
            

#Clase que implementa los metodos de la interfaz, es decir sus metodos callback
class Handler():
    def __init__(self):
        self.cont = 0
        self.user = Usuario(0,"","",0,"")
        
    #Funcion que permite guardar los cambios al agregar una persona al Sistema
    def btnAceptar_clicked_cb(self, widget):
        dialog = Gtk.MessageDialog(widget, 0, Gtk.MessageType.QUESTION,
            Gtk.ButtonsType.YES_NO, "Desea agregar otra persona al sistema?")
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            vtnAdmUser.txtNomb.set_text("")              
            vtnAdmUser.txtApell.set_text("")
            vtnAdmUser.txtGETID.set_text(self.cont)
            self.cont = self.cont+1
            
        elif response == Gtk.ResponseType.NO:
            print("QUESTION dialog closed by clicking NO button")
            widget.destroy()

        dialog.destroy()

    def btnCancelar_clicked_cb(self, widget):
        dialog = Gtk.MessageDialog(widget, 0, Gtk.MessageType.QUESTION,
            Gtk.ButtonsType.YES_NO, "Desea guardar los cambios")
        
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            print "Se quiso guardar"
            widget.destroy()
        elif response == Gtk.ResponseType.NO:
            print("QUESTION dialog closed by clicking NO button")
            widget.destroy()

        dialog.destroy()

    def btnGenerar_clicked_cb(self, widget, data = None):
        clav = Clave()
        self.user.claveNum = clav.num
        self.user.claveAlfNum = clav.alfa        
        
    def btnUser_clicked_cb(self, widget, data = None):
        widget.show_all()
        
    def btnAdmin_clicked_cb(self, widget):       
        widget.show_all()
    
    def txtApell_activate_cb(self, entry, data = None):
          self.user.setNombre( entry.get_text() )
    
    def txtNomb_activate_cb(self, entry, data = None):
        self.user.setApellido( entry.get_text() )

    def btnTomarFoto_activate_cb(self, widget, data = None):
        pass        

#builder.connect_signals(Handler())        

arc = ArchiveHandler()

builder = Gtk.Builder()
#builder.add_from_file(UI_FILE)
#builder.connect_signals(Handler())    
#vtnPrinc = builder.get_object('VentanaPrincipal')
#vtnPrinc.connect("delete-event", Gtk.main_quit)

#if Gtk.get_current_event_state():
#   print "Has presionado un boton"

#vtnUser = builder.get_object('VtnUser')
#vtnAdmUser = builder.get_object('VtnAdmUser')     

vtn = VtnAdmUser.VtnAdmUser()
vtn.vtnAdmUser.show_all()

#vtnPrinc.show_all()

Gtk.main()


#arc.escribirArchivo(USUARIO_FILE)

arc.leerArchivo(USUARIO_FILE)

for usuario in UsuarioList:
    print usuario.getNombre()
 