# -*- coding: utf-8 -*-
"""
Created on Sun Dec  7 05:27:26 2014

@author: kevin
"""

#!/usr/bin/env python

"""
foto_webcam.py:
A simple tool for taking snapshots from webcam. The images are saved in the 
current directory named 1.jpg, 2.jpg, ...

Usage:
    Press [SPACE] to take snapshot
    Press 'q' to quit
"""

import cv2

cascPath = "haarcascade_frontalface_default.xml"

def tomar_foto( iden, delay=2):
    
    faceCascade = cv2.CascadeClassifier(cascPath)

    cap = cv2.VideoCapture(0)
    
    if not cap.isOpened():
        print "Cannot open camera!"
        return

    # Set video to 320x240
    cap.set(3, 640) 
    cap.set(4, 480)

    take_picture = False;
    t0, filenum = 0, 1
    
    ruta_foto = str(iden) + ".jpg"

    while True:
        val, frame = cap.read()
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags=cv2.cv.CV_HAAR_SCALE_IMAGE
        )
    
        # Draw a rectangle around the faces
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        
        key = cv2.waitKey(1)
        
        cv2.imshow("Video", frame)
        
        if key & 0xFF == ord('t'):
            t0 = cv2.getTickCount()
            take_picture = True
        elif key & 0xFF == ord('q'):
            break
        
        
        if take_picture and ((cv2.getTickCount()-t0) / cv2.getTickFrequency()) > delay:
            
            cv2.imwrite(ruta_foto, frame)
            filenum += 1
            take_picture = False
        
        
    cap.release()    
    cv2.destroyAllWindows()
    return ruta_foto

if __name__ == "__main__":
  tomar_foto()
  cv2.destroyAllWindows()